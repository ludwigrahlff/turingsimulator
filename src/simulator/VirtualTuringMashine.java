package simulator;
import java.util.ArrayList;
import java.util.Iterator;

public class VirtualTuringMashine {
	private int nrOfStates = 0;
	private int nrOfBSymb = 3;
    private boolean finishingStates[];
    private ArrayList<Relations> relations;
    private RelationEnd[][] relationarr = null;
    public RelationEnd emptyRel;
    
    
    public VirtualTuringMashine(int nrOfStates) {
    	this.nrOfStates=nrOfStates;
    	this.relations = new ArrayList<Relations>();
    	emptyRel = new RelationEnd(0,0,0,0);
    	finishingStates = new boolean[nrOfStates];
		for(int j =0; j<nrOfStates; j++) {
			finishingStates[j]=false;
		}
    };
    
    
    public boolean addRelation(int p, int a, int q, int b, int d, int nroftrans) {
    	if(p>0 && q>0 && p<= this.nrOfStates && q <= this.nrOfStates && a<=3 && a>0 && b<=3 && b>0 && d<=3 && d>0) {
    		this.relations.add(new Relations(p, a, q, b, d, nroftrans));
    		return true;
    	}
		return false;
    };
    
    public boolean makeStateFinishing(int q) {
    	if(q<= this.nrOfStates && q > 0) {
    			if(finishingStates[q-1]) {
    				return false;
    			}
    			finishingStates[q-1] = true;
    			return true;
    	}
    	return false;
    }
    public void changeofSym(int nr) {
    	this.nrOfBSymb=nr;
    }
    
    public void convertRelations() {
    	relationarr = new RelationEnd[nrOfStates][nrOfBSymb];
    	for(int i =0; i<nrOfStates; i++) {
    		for(int j =0; j<nrOfBSymb; j++) {
    			relationarr[i][j]=emptyRel;
    		}
    	}
    	
    	Iterator<Relations> iter = relations.iterator();
    	while(iter.hasNext()) {
    		Relations rel = iter.next();
    		relationarr[rel.p-1][rel.a-1] = new RelationEnd(rel.q, rel.b, rel.d, rel.nroftrans);
    	}
    }
    public boolean isfinishedState(int q) {
    	if(q<=finishingStates.length) {
    		return finishingStates[q-1];
    	}
    	return false;
    }
    
    public RelationEnd getRel(int p, int a) {
    	return relationarr[p-1][a-1];
    }
    
    public int getStates() {
    	return nrOfStates;
    }


	public void printFinish() {
		for(int j =0; j<nrOfStates; j++) {
			System.out.println(finishingStates[j]);
		}
		
	}
	
	public void printRelationTable() {
		String output = "";
		output = output + "| " + VirtualTuringMashine.getlong("", Integer.toString(nrOfStates).length(), ' ', false) + "p  ";
		output = output + "| "  + "GR" + " ";
		output = output + "| " + VirtualTuringMashine.getlong("", Integer.toString(nrOfStates).length(), ' ', false) + "q  ";
		output = output + "| "  + "GW" + " ";
		output = output + "| "  + "DM" + " |\n";
		Iterator<Relations> iter = relations.iterator();
    	while(iter.hasNext()) {
    		Relations rel = iter.next();
    		output = output + "| q" + VirtualTuringMashine.getlong(Integer.toString(rel.p), Integer.toString(nrOfStates).length(), ' ', false) + "  ";
    		output = output + "| "  + Configuration.getchar(rel.a) + "  ";
    		output = output + "| q" + VirtualTuringMashine.getlong(Integer.toString(rel.q), Integer.toString(nrOfStates).length(), ' ', false) + "  ";
    		output = output + "| "  + Configuration.getchar(rel.b) + "  ";
    		output = output + "| "  + Configuration.getdir(rel.d) + "  |\n";
    	}
    	System.out.println(output);
	}
	public void generateTikZCode() {
		String output = "";
		output = output + "\\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick]\n" + 
						  "\t\\tikzstyle{every state}=[]\n";
		
		int i = 1;
		while(i<=nrOfStates) {
			output = output+ "\t\\node[state" + (i==1?", initial":"") + (finishingStates[i-1]?", accepting":"") + "]";
			output = output+ "\t(" + i + ") " + (i!=1? ("[right = of " + (i-1) + "]"):"") + "{$q_{" + i + "}$};";
			output = output+ "\n";
			i++;
		}
		output = output+ "\n\t\\path \n";
		Iterator<Relations> iter = relations.iterator();
		while(iter.hasNext()) {
    		Relations rel = iter.next();
			output = output + "\t\t(" + rel.p + ")\tedge[bend left]\tnode{$";
			output = output + (Configuration.getchar(rel.a)=='⊔'?"\\sqcup":Configuration.getchar(rel.a)) + "/"+ (Configuration.getchar(rel.b)=='⊔'?"\\sqcup":Configuration.getchar(rel.b)) + "/"+ Configuration.getdir(rel.d);
			output = output + "$}\t(" + rel.q + ")\n";
    		
    	}
		output = output + "\t; \n";
		
		output = output + "\\end{tikzpicture}\n\n";
    	System.out.println(output);
	}
	public void generateTikZCode2() {
		String output = "";
		output = output + "\\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick]\n" + 
						  "\t\\tikzstyle{every state}=[]\n";
		
		int i = 1;
		while(i<=nrOfStates) {
			output = output+ "\t\\node[state" + (i==1?", initial":"") + (finishingStates[i-1]?", accepting":"") + "]";
			output = output+ "\t(" + i + ") " + (i!=1? ("[right = of " + (i-1) + "]"):"") + "{$q_{" + i + "}$};";
			output = output+ "\n";
			i++;
		}
		
		ArrayList<RelationOnlySymbAndDir>[][] relat = new ArrayList[nrOfStates][nrOfStates];
		for(int m = 0; m<relat.length; m++) {
			for(int n = 0; n<relat[0].length; n++) {
				relat[m][n] = new ArrayList<RelationOnlySymbAndDir>();
			}
		}
		
		
		//generate an array
		output = output+ "\t\\path \n";
		Iterator<Relations> iter = relations.iterator();
		int j = 0;
		while(iter.hasNext()) {
			j++;
    		Relations rel = iter.next();
    		relat[rel.p-1][rel.q-1].add(new RelationOnlySymbAndDir(rel.a, rel.b, rel.d, j));
    	}
		
		for(int m = 0; m<relat.length; m++) {
			for(int n = 0; n<relat[0].length; n++) {
				Iterator<RelationOnlySymbAndDir> iterin = relat[m][n].iterator();
				if (relat[m][n].size()>=2) {
					output = output + "\t\t(" + (m+1) + ")\tedge" + (m==n?"[loop above]":"[bend left]") + "\tnode{$";
					output = output + "\\begin{array}{c} ";
					while(iterin.hasNext()) {
						RelationOnlySymbAndDir rela = iterin.next();
						output = output + (Configuration.getchar(rela.a)=='⊔'?"\\sqcup":Configuration.getchar(rela.a)) + "/";
						output = output + (Configuration.getchar(rela.b)=='⊔'?"\\sqcup":Configuration.getchar(rela.b)) + "/";
						output = output + Configuration.getdir(rela.d);
						output = output + " \\\\ ";
					}
					output = output + "\\end{array} ";
					output = output + "$}\t(" + (n+1) + ")\n";
				} else if(relat[m][n].size()==1) {
					RelationOnlySymbAndDir rela = iterin.next();
					output = output + "\t\t(" + (m+1) + ")\tedge" + (m==n?"[loop above]":"[bend left]") + "\tnode{$";
					output = output + (Configuration.getchar(rela.a)=='⊔'?"\\sqcup":Configuration.getchar(rela.a)) + "/";
					output = output + (Configuration.getchar(rela.b)=='⊔'?"\\sqcup":Configuration.getchar(rela.b)) + "/";
					output = output + Configuration.getdir(rela.d);
					output = output + "$}\t(" + (n+1) + ")\n";
				}
			}
		}
		
		
		
		output = output + "\t; \n";
		output = output + "\\end{tikzpicture}\n\n";
    	System.out.println(output);
	}
	
	public static String getlong(String str, int n, char c, boolean front) {
		while(str.length()<n) {
			str = "" + (front?c:"") + str + (front?"":c);
		}
		return str;
	}
    
}
