package simulator;

public class Configuration {
	private VirtualTuringMashine vtm = null;
	private String lhs = "";
	private String rhs = "";
	private int state = 1;
	private boolean hasfinished = false;
	
	public Configuration(String word, VirtualTuringMashine vtm) {
		this.rhs = buildWord(word) + "⊔⊔";
		this.lhs = "⊔⊔";
		this.vtm = vtm;
	}
	
	public boolean nextConf() {
		String before = "(" + lhs + ", " + VirtualTuringMashine.getlong(Integer.toString(state), Integer.toString(vtm.getStates()).length(), ' ', true) + ", " + rhs + ") |-";
		if(vtm.getRel(state, getkey(rhs.charAt(0)))==vtm.emptyRel) {
			return false;
		} else {
			RelationEnd getEnd = vtm.getRel(state, getkey(rhs.charAt(0)));
			if (getEnd.d==1) {
				rhs = "" + lhs.charAt(lhs.length()-1) + getchar(getEnd.b) + rhs.substring(1);
				if (lhs.length()<=2) {
					lhs = '⊔' + lhs.substring(0,lhs.length()-1);
				} else {
					lhs = lhs.substring(0, lhs.length()-1);
				}
				state = getEnd.q;
			} else if (getEnd.d==2) {
				if (rhs.length()<=2) {
					rhs = rhs.substring(1) + '⊔';
				} else {
					rhs = rhs.substring(1);
				}
				lhs = lhs + getchar(getEnd.b);
				state = getEnd.q;
			} else if (getEnd.d==3) {
				state = getEnd.q;
				rhs = getchar(getEnd.b) + rhs.substring(1);
			} else {
				return false;
			}
		}
		hasfinished = hasfinished || this.isFinished();
		System.out.println(before + "(" + lhs + ", " + VirtualTuringMashine.getlong(Integer.toString(state), Integer.toString(vtm.getStates()).length(), ' ', true) + ", " + rhs + ") : " + (hasfinished?"Fini":"NotF"));
		return true;
	}
	
	public boolean isFinished() {
		return vtm.isfinishedState(state);
	}
	
	public boolean hasFinished() {
		return this.hasfinished;
	}
	
	public void updateFinished() {
		hasfinished = hasfinished || this.isFinished();
	}
	
	
	private String buildWord(String word) {
		word.replaceAll("000", "⊔");
		word.replaceAll("00", "1");
		word.replaceAll("0", "0");
		word.replaceAll("1", "");
		return word;
	}
	
	public static int getkey(char a) {
		switch (a) {
		case '0': return 1;
		case '1': return 2;
		case '⊔': return 3;
		}
		return 0;
	}
	
	public static char getchar(int a) {
		switch (a) {
		case 1: return '0';
		case 2: return '1';
		case 3: return '⊔';
		}
		return 'a';
	}
	
	public static char getdir(int d) {
		switch (d) {
		case 1: return 'l';
		case 2: return 'r';
		case 3: return 'n';
		}
		return 'a';
	}
	
	public String getlhs() {
		return lhs;
	}
	
	public String getrhs() {
		return rhs;
	}

}
