package simulator;

public class RelationEnd {

	int nroftrans;
	int q;
	int b;
	int d;
	
	public RelationEnd(int q, int b, int d, int nroftrans) {
		this.b=b;
		this.q=q;
		this.d=d;
		this.nroftrans=nroftrans;
	}
	
	public void printRelEnd() {
		System.out.println("-> (" + q +", " + b + ", "+ d + ") :" + nroftrans);
	}
}
