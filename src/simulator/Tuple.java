package simulator;

public class Tuple<S1, S2> {
	private S1 first = null;
	private S2 second = null;
	
	public Tuple(S1 fir, S2 sec){
		this.first= fir;
		this.second= sec;
	}

	public S1 first() {
		return first;
	}
	public S2 second() {
		return second;
	}
}
