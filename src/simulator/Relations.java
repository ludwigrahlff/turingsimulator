package simulator;

public class Relations {
	int p;
	int a;
	int q;
	int b;
	int d;
	int nroftrans;
	
	public Relations(int p, int a, int q, int b, int d, int nroftrans) {
		this.p=p;
		this.a=a;
		this.b=b;
		this.q=q;
		this.d=d;
		this.nroftrans=nroftrans;
	}
	
	public void printRelation() {
		System.out.println("" + nroftrans + ": (" + p + ", " + a + ") -> (" + q + ", " + b + ", " + d + ")" );
	}
	
}