package simulator;

import java.util.Scanner;

public class main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
//		String input = args[0];
		if(!check4format(input, 0)) {
			System.out.println("Didnt got a correct w#x");
		} else {
			String arr[] = getbothwords(input);
			String machinecoded = arr[0];
			String word = arr[1];
			Tuple<Boolean, VirtualTuringMashine> tuple = createVTM(machinecoded);
			VirtualTuringMashine vtm = tuple.second();
			if(!tuple.first()) {
				System.out.println("Failed to generate the VirtualMashine");
			}else {
				int max = scanner.nextInt();
				String waste = scanner.nextLine();
				vtm.printRelationTable();
				vtm.generateTikZCode();
				vtm.generateTikZCode2();
				Configuration tmconf = new Configuration(word, vtm);
				boolean terminated = false;
				int j = 0;
				
				
				
				while(!terminated) {
					int i =0;
					boolean test = (i<max) && true;
					
					
					while(test) {
						tmconf.updateFinished();
						i++;
						terminated = !tmconf.nextConf();
						test = i<max && !terminated;
					}
					max = 20;
					if (terminated) break;
					j++;
					System.out.println(j);
					waste = scanner.nextLine();
				}
				System.out.println("TM terminated: Wort wurde " + (tmconf.hasFinished()?"":"nicht ") + "akzeptiert.");
				String bandende = tmconf.getlhs() + tmconf.getrhs();
				int count0 = 0;
				for (int i = 0; i < bandende.length(); i++) {
				    if (bandende.charAt(i) == '0') {
				        count0++;
				    }
				}
				int count1 = 0;
				for (int i = 0; i < bandende.length(); i++) {
				    if (bandende.charAt(i) == '0') {
				        count1++;
				    }
				}
				System.out.println(bandende.replaceAll("⊔", ""));
				System.out.println("0:" + count0 + " 1: "+ count1);
			}
			
		}
	}
	
	
	
	
	public static boolean check4format(String in, int nrofhash) {
		if (in.length()==0 && nrofhash==1){
			return true;
		}else if (in.length()==0) {
			return false;
		}else if (in.charAt(0)=='#'){
			return check4format(in.substring(1, in.length()), nrofhash+1);
		}else if (in.charAt(0)=='0' || in.charAt(0)=='1'){
			return check4format(in.substring(1, in.length()), nrofhash);
		} else return false;
	}
	
	public static String[] getbothwords (String input){
		int poshash = input.indexOf('#');
		String bothwords[] = new String[2];
		bothwords[0] = input.substring(0, poshash);
		bothwords[1] = input.substring(poshash+1);
		return bothwords;
	}
	
	public static Tuple<Boolean, VirtualTuringMashine> createVTM(String tm) {
		
		String[] onlyzeros = tm.split("1");
		int[] nrofzeros = new int[onlyzeros.length];
//		System.out.println(onlyzeros.length);
		for(int i =0 ; i<nrofzeros.length; i++) {
			nrofzeros[i] = onlyzeros[i].length();
//			System.out.println("anzahl 0en: " + nrofzeros[i] + " zeug: " + onlyzeros[i] + "  :" + onlyzeros[i].length());
		}
		VirtualTuringMashine vm = null;
		int posone = tm.indexOf('1');
		if(posone<0)return new Tuple<Boolean, VirtualTuringMashine>(false, vm);
		vm = new VirtualTuringMashine(posone);
		tm = tm.substring(posone+1);
		
		posone = tm.indexOf('1');
		if(posone<0)return new Tuple<Boolean, VirtualTuringMashine>(false, vm);
		vm.changeofSym(posone);
		tm = tm.substring(posone+1);
		
//		System.out.println("make states finished");
		
		int nroffinishedStates = tm.indexOf('1');
		if(nroffinishedStates<0)return new Tuple<Boolean, VirtualTuringMashine>(false, vm);
		tm = tm.substring(nroffinishedStates+1);
		while(nroffinishedStates>=1) {
			posone = tm.indexOf('1');
			if(posone<0)return new Tuple<Boolean, VirtualTuringMashine>(false, vm);
			vm.makeStateFinishing(posone);
			tm = tm.substring(posone+1);
			nroffinishedStates--;
		}
//		System.out.println("now the relations");
		int i =0;
		while(tm.length()>=1) {
			i++;
			posone = tm.indexOf('1');
			if(posone<0)return new Tuple<Boolean, VirtualTuringMashine>(false, vm);
			tm = tm.substring(posone+1);
			int p = posone;
			posone = tm.indexOf('1');
			if(posone<0)return new Tuple<Boolean, VirtualTuringMashine>(false, vm);
			tm = tm.substring(posone+1);
			int a = posone;
			posone = tm.indexOf('1');
			if(posone<0)return new Tuple<Boolean, VirtualTuringMashine>(false, vm);
			tm = tm.substring(posone+1);
			int q = posone;
			posone = tm.indexOf('1');
			if(posone<0)return new Tuple<Boolean, VirtualTuringMashine>(false, vm);
			tm = tm.substring(posone+1);
			int b = posone;
			posone = tm.indexOf('1');
			if(posone<0)return new Tuple<Boolean, VirtualTuringMashine>(false, vm);
			tm = tm.substring(posone+1);
			int d = posone;
			
			vm.addRelation(p, a, q, b, d, i);
		}
		vm.convertRelations();
		return new Tuple<Boolean, VirtualTuringMashine>(true, vm);
	}
}
