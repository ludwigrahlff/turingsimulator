package simulator;

public class RelationOnlySymbAndDir {

	int nroftrans;
	int a;
	int b;
	int d;
	
	public RelationOnlySymbAndDir(int a, int b, int d, int nroftrans) {
		this.a=a;
		this.b=b;
		this.d=d;
		this.nroftrans=nroftrans;
	}
	
	public void printRelSymbAndDir() {
		System.out.println("-> (" + a +", " + b + ", "+ d + ") :" + nroftrans);
	}
}
